package Queries;

public class Bus_Query {

	public String getSchemaStructure(){
		return "{" + "\"query\"" + ":"+"\""
				+"{"
				  + "__schema{"
				  	+"queryType{"
				  		+"name,"
				  		+"fields {"
				  			+"name,"
				  			+"type {"
				  				+"kind,"
				  				+"ofType {"
				  					+"name,"
				  					+"description,"
				  					+"fields {"
				  						+"name"
				  					+"}"
			  					+"}"
		  					+"}"
						+"}"
					+"}"
			      +"}"
			    + "}\""
			+ "}";
	}
	
	public String randomQuery01(){
		return  "{" + "\"query\"" + ":"+"\""
			+ "{"
				+ "Santander(Direccion: "+"\\"+"\"Lluja"+"\\"+"\" Parada: "+"\\"+"\"Ines Diego Del Noval 25"+"\\"+"\"){"
					+ "Bus,"
					+ "Parada,"
					+ "Longitud,"
					+ "Direccion,"
					+ "Latitud"
				+ "},"
				+ " Barcelona(Bus:"+"\\"+"\"BUS -60--"+"\\"+"\" limit:1){"
					+ "Bus,"
					+ "Parada,"
					+ "Longitud,"
					+ "Latitud"
				+ "},"
				+ "Caceres(limit:3 Direccion: "+"\\"+"\"Vuelta"+"\\"+"\" Parada: "+"\\"+"\"Ciudad Deportiva De La Junta De Extremadura"+"\\"+"\"){"
					+"Parada,"
					+"Bus,"
					+"Direccion"
					+"},"
				+ "Malaga(Direccion:"+"\\"+"\"Churriana"+"\\"+"\" ){"
					+ "Bus,"
					+ "Parada,"
					+ "Longitud,"
					+ "Direccion,"
					+ "Latitud"
					+"}"
			+ "}\""
		+ "}";
	}
	
	public String randomQuery02(){
		return "{" + "\"query\"" + ":"+"\""
			+ "{"
				+ "Barcelona(Parada: "+"\\"+"\"Sant Andreu"+"\\"+"\"){"
					+ "Parada,"
					+ "Bus"
				+ "},"
				+ " Caceres(Bus:"+"\\"+"\"RC - AV. I. DE MOCTEZUMA-CAMPUS UNIV."+"\\"+"\"){"
					+ "Parada,"
					+ "Direccion"
				+ "}"
			+ "}\""
		+ "}";
	}

	public String randomQuery03(){
		return "{" + "\"query\"" + ":"+"\""
			+ "{"
				+"Barcelona(limit:4){"
					+"Bus,"
					+"Parada,"
					+"Longitud,"
					+"Latitud"
				+"},"
				+"Bicicletas_Barcelona(limit:3){"
				+"Bici,"
				+"Calle,"
				+"Longitud,"
				+"Latitud,"
				+"NumeroBicis,"
				+"Espacio,"
				+"Cercanas,"
				+"Estado"
			+"}"
			+ "}\""
		+ "}";
	}

	public String getAll(){
		return "{" + "\"query\"" + ":"+"\""
				+"{"
				  + "Barcelona{"
				  	+"Bus,"
				  	+"Parada,"
				  	+"Longitud,"
				  	+"Latitud"
				  +"},"
				  + "Caceres{"
				  	+"Bus,"
				  	+"Parada,"
				  	+"Longitud,"
				  	+"Latitud,"
				  	+"Direccion"
				  +"},"
				  + "Malaga{"
				  	+"Bus,"
				  	+"Parada,"
				  	+"Longitud,"
				  	+"Latitud,"
				  	+"Direccion"
				  +"},"
				  + "Santander{"
				  	+"Bus,"
				  	+"Parada,"
				  	+"Longitud,"
				  	+"Latitud,"
				  	+"Direccion"
				  +"},"
				  +"Bicicletas_Barcelona{"
					+"Bici,"
					+"Calle"
					+"Longitud"
					+"Latitud"
					+"NumeroBicis"
					+"Espacio"
					+"Cercanas"
					+"Estado"
				  +"}"
			    + "}\""
			+ "}";
	}
}
