package Parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Iterator;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class model_parser {

	private String model_includes="let { \n"
						   +" GraphQLString,\n"
						   +" GraphQLInt, //distances \n"
						   +" GraphQLObjectType, \n"
						   +" GraphQLBoolean, \n"
						   +" GraphQLFloat \n"
						   +"} = require('graphql');\n\n"
						   +"const BusType = new GraphQLObjectType({\n";
	
	private String Schema_includes="let {\n"
							+ "  GraphQLList,\n"
							+ "  GraphQLObjectType,\n"
							+ "  GraphQLSchema,\n"
							+ "  GraphQLNonNull,\n"
							+ "  GraphQLString,\n"
							+ "  GraphQLInt,\n"
							+ "  GraphQLFloat,\n"
							+ "} = require('graphql');\n\n"
							
							+"const _ = require('lodash'); \n"
							+"const rp = require('request-promise');\n";
	
	private String Schema_query="\nconst BusQuery = new GraphQLObjectType({\n"
 		   						+"    name: 'Bus_Query',\n"
 		   						+"    description: 'query',\n"
 		   						+"	fields: () =>({\n";
	
	private String Schema_export="		}\n"
								+"	})\n"
								+"})\n"
								+"const Bus_Schema = new GraphQLSchema({\n"
								+"	query: BusQuery\n"
								+"});\n"
								+"module.exports= Bus_Schema;\n";
	
	private String Schema_last_field="				if(typeof args.limit !== \"undefined\"){\n"
								   +"					bool=true;\n"
								   +"					if(response.length>0){\n"
								   +"						results=[];\n"
								   +"						if (response.length>args.limit){\n"
								   +"							for (i = 0; i < args.limit; i++)\n"
								   +"								results=results.concat(response[i]);\n"
								   +"							response=results;\n"
								   +"						}\n"
								   +"					}else{\n"
								   +"						if (results.length>args.limit)\n"
								   +"							for (i = 0; i < args.limit; i++)\n"
								   +"								response=response.concat(results[i]);\n"
								   +"						else\n"
								   +"							response=results;\n"
								   +"					}\n"
								   +"				}\n"
								   +"				if (bool)\n"
								   +"					return response;\n"
								   +"				else\n"
								   +"					return results;\n"
								   +"			})\n";
	public void init(){
		
		File dir_in = new File ("./Parser_JSON");
	
		File [] json_in = dir_in.listFiles(new FilenameFilter(){
			public boolean accept( File dir, String name){
				return name.endsWith("json");
			}
		});

		//lamada al parser
		parser(json_in);
	}
	
	private void parser(File[] files){
		JSONParser parser = new JSONParser();
		JSONObject jsonObject = new JSONObject();
		String name=new String();
		String description=new String();
		JSONObject fields = new JSONObject();
		JSONObject query = new JSONObject();
        try
        {

            FileWriter schema_fw = new FileWriter("./Parser_JS/Bus_Schema.js");
            schema_fw.write(Schema_includes);
        	for (int i=0; i<files.length; i++)//necesario primer for para dependencias en el schema
        		schema_fw.write("const "+files[i].getName().substring(0, files[i].getName().indexOf("."))+" = require ('../Models/"+files[i].getName().substring(0, files[i].getName().indexOf("."))+".js');\n");
    		
        	schema_fw.write(Schema_query);
        	
        	for (int i=0; i<files.length; i++){
        		
        		
        		Object object = parser.parse(new FileReader(files[i].getAbsolutePath()));
                //convert Object to JSONObject
                jsonObject = (JSONObject)object;
                //Reading the Strings
                
                name = (String) jsonObject.get("Name");
                description = (String) jsonObject.get("Description");
                
                //Reading the arrays
                fields = (JSONObject) jsonObject.get("fields");
            	FileWriter model_fw = new FileWriter("./Parser_JS/"+files[i].getName().substring(0, files[i].getName().indexOf("."))+".js");
            	model_fw.write(model_includes);
            	model_fw.write("	name: '"+name+"', \n"
            				  +"	description: '"+description+"', \n"
            				  +"	fields: () => ({  \n");
            	Iterator it = fields.keySet().iterator();
            	while (it.hasNext()){//itero todos los campos que tenga el json
            		Object ob= it.next();
            		model_fw.write("		"+ob.toString()+":{\n");
            		model_fields(model_fw, (JSONObject) fields.get(ob.toString()));
            		if (it.hasNext())
            			model_fw.write("    	},\n");
            		else
            			model_fw.write("    	}\n");
            	}
            	model_fw.write("   })\n"
		            		  +"})\n"
		            		  +"module.exports= BusType;");
            	
            	model_fw.close();
            	
            	//UNA VEZ TERMINADO EL MODELO SE A�ADE LA PARTE DEL SCHEMA

                query = (JSONObject) jsonObject.get("Query");
                schema_fw.write("		"+name+":{\n"
            					+"			type: new GraphQLList("+files[i].getName().substring(0, files[i].getName().indexOf("."))+"),\n"
            					+"			args:{\n"
            					+"				limit:{type:GraphQLInt},\n");
                it = fields.keySet().iterator();
            	while (it.hasNext()){
            		Object ob= it.next();
            		schema_fw.write("				"+ob.toString()+":{");
            		if (ob.toString().contains("Lon"))
            			schema_fw.write("type:GraphQLFloat}");
            		else if (ob.toString().contains("Lat"))
            			schema_fw.write("type:GraphQLFloat}");
            		else
            			schema_fw.write("type:GraphQLString}");
            		if (it.hasNext())//ASEGURAMOS QUE NO PONE LA , AL FINAL
            			schema_fw.write(",\n");
            		else
            			schema_fw.write("\n");
            	}
            	schema_fw.write(" 			},\n"
            					+" 			resolve: (_,args) => rp({\n"
            					+" 				uri: `"+query.get("uri")+"`,\n");
            	if (query.get("endpoint").toString().equals("datastore")){
            		schema_fw.write(" 				qs:{\n"
            						+"					resource_id: '"+query.get("resource_id")+"'\n"
            						+"				}\n");
            	}
            	else if (query.get("endpoint").toString().equals("SparQL")){
            		schema_fw.write(" 				qs:{\n"
            						+"					query: '"+query.get("query")+"',\n"
            						+"					format:'json',\n"
            						+"				}\n");
            	}
            	schema_fw.write("			}).then(function(responseJson) {\n"
            					+"				var parsed = JSON.parse(responseJson);\n"
            					+"				var results = parsed."+query.get("results")+";\n"
            					+"				var response=[];\n"
            					+"				var bool=false;\n");
            	it = fields.keySet().iterator();
            	while (it.hasNext()){
            		Object ob= it.next();
            		
            		schema_fw.write("				if (typeof args."+ob.toString()+" !== \"undefined\"){\n"
            						+"					bool=true;\n"
            						+"					if (response.length>0){\n"
            						+"						results=[];\n"
            						+"						for (i = 0; i < response.length; i++)\n"
            						+"							if (response[i]");
            		JSONObject aux= (JSONObject) fields.get(ob.toString());
            		
            		if (aux.containsKey("search"))//si tenemos un campo compuesto se busca por el parametro search del json
            			schema_fw.write("."+aux.get("search"));
            		else if (aux.get("name").toString().contains("[")) //casos muy especiales
            			schema_fw.write(aux.get("name").toString().substring(0,aux.get("name").toString().length()));
            		else
            			schema_fw.write("."+aux.get("name"));
            		
            		if ((ob.toString().startsWith("Direccion")) && (aux.containsKey("resolve"))){
            			schema_fw.write(" == true){\n"
            						   +"								if (args."+ob.toString()+" == ");
            			aux=(JSONObject) aux.get("resolve");
            			if (aux.get("true").toString().contains("'"))//si comparamos con un string en vez de obj
            				schema_fw.write(aux.get("true")+")\n");
            			else
            				schema_fw.write("response[i]."+aux.get("true")+")\n");
            			schema_fw.write("									results=results.concat(response[i]);\n"
            							+"							}\n"
            							+"							else if (args."+ob.toString()+" == ");
            			if (aux.get("true").toString().contains("'"))//si comparamos con un string en vez de obj
            				schema_fw.write(aux.get("false")+")\n");
            			else
            				schema_fw.write("response[i]."+aux.get("false")+")\n");
            			
            		}else
            			schema_fw.write(" == args."+ob.toString()+")\n");
            			
        			schema_fw.write("								results=results.concat(response[i]);\n"
        					   +"						response=results;\n"
        					   +"					}else{\n"
        					   +"						for (i = 0; i < results.length; i++)\n"
        					   +"							if (results[i]"); 
            		aux= (JSONObject) fields.get(ob.toString());
            		if (aux.containsKey("search"))//si tenemos un campo compuesto se busca por el parametro search del json
            			schema_fw.write("."+aux.get("search"));
            		else if (aux.get("name").toString().contains("[")) //casos muy especiales
            			schema_fw.write(aux.get("name").toString().substring(0,aux.get("name").toString().length()));
            		else
            			schema_fw.write("."+aux.get("name"));
            		
            		if ((ob.toString().startsWith("Direccion")) && (aux.containsKey("resolve"))){
            			schema_fw.write(" == true){\n"
            						   +"								if (args."+ob.toString()+" == ");
            			aux=(JSONObject) aux.get("resolve");
            			if (aux.get("true").toString().contains("'"))//si comparamos con un string en vez de obj
            				schema_fw.write(aux.get("true")+")\n");
            			else
            				schema_fw.write("results[i]."+aux.get("true")+")\n");
            			schema_fw.write("									response=response.concat(results[i]);\n"
            							+"								}\n"
            							+"							else if (args."+ob.toString()+" == ");
            			if (aux.get("true").toString().contains("'"))//si comparamos con un string en vez de obj
            				schema_fw.write(aux.get("false")+")\n");
            			else
            				schema_fw.write("results[i]."+aux.get("false")+")\n");
            			
            		}else
            			schema_fw.write(" == args."+ob.toString()+")\n");

            		schema_fw.write("								response=response.concat(results[i]);\n"
            						+"						results=response;\n"
            						+"					}\n"
            					    +"				}\n"); 
            	}
        		schema_fw.write(Schema_last_field);
            	
            	if (i < files.length-1)
            		schema_fw.write("		},\n");
        	}
        	schema_fw.write(Schema_export);

        	schema_fw.close();
        }
        catch(FileNotFoundException fe)
        {
            fe.printStackTrace();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
	}
	
	private void model_fields(FileWriter model_fw, JSONObject aux){

		JSONObject aux2= new JSONObject();
		
		try {
			if (aux.get("name").toString().contains("["))
    			model_fw.write("        	name:'"+aux.get("name").toString().substring(2,aux.get("name").toString().length()-2)+"',\n");
			else
				model_fw.write("            name:'"+aux.get("name")+"',\n");
			model_fw.write("            type:"+aux.get("type")+",\n"
						  +"            description:'"+aux.get("desc")+"',\n"
						  +"            resolve(obj) {\n");
	    	if (aux.containsKey("resolve")){
	    		aux2=(JSONObject) aux.get("resolve");
	    		model_fw.write("			if (obj."+aux.get("name")+"==true)\n");
	    		if (aux2.get("true").toString().startsWith("'")) //si es un string
	    			model_fw.write("   				return "+aux2.get("true")+"\n");
	    		else//si es un obj
	    			model_fw.write("   				return obj."+aux2.get("true")+"\n");
	    		
	    		model_fw.write("       		else\n");
	    		
	    		if (aux2.get("false").toString().startsWith("'"))
	    			model_fw.write("        		return "+aux2.get("false")+"\n");
	    		else
	    			model_fw.write("       			return obj."+aux2.get("false")+"\n");
	    	}else{
	    		if (aux.get("name").toString().contains("["))
	    			model_fw.write("              return obj"+aux.get("name")+"\n");
	    		
	    		else if (aux.get("name").toString().contains("+"))
	    			model_fw.write("        		return obj."+aux.get("name").toString().substring(0,aux.get("name").toString().indexOf("+")-1)+"+\"  \"+obj."+aux.get("name").toString().substring(aux.get("name").toString().indexOf("+")+2,aux.get("name").toString().length())+"\n");
	    		else
	    			model_fw.write("        		return obj."+aux.get("name")+"\n");
	    		
	    	}
	    	model_fw.write("            }\n");
	    	
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
