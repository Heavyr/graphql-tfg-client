import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Scanner;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import Parser.model_parser;
import Queries.Bus_Query;
public class Client {

	/* URL de el servicio */
	private int portNumber=0;
	private String URL_STRING = "http://localhost:"+portNumber+"/graphiql";
	private Bus_Query bq= new Bus_Query();
	/* Query a GraphQL */
	private void sendPost(String message) throws Exception {

		HttpClient client = HttpClientBuilder.create().build();
		HttpPost post 	  = new HttpPost(URL_STRING);

		//cabecera http json 
		post.addHeader("Content-Type", "application/json");
		
		// peticion json con la query
		StringEntity body = new StringEntity(message);
		post.setEntity(body);
				
		HttpResponse response = client.execute(post);
		
		// codigo de estado
		System.out.println(response.getStatusLine());

		// respuesta del servidor
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		StringBuffer result = new StringBuffer();
		String line = "";
		
		while ((line = rd.readLine()) != null) {
			result.append(line + "\n");
		}

		System.out.println(result.toString());
	}
	
	private void choose(){
		
		System.out.println("--------------- Opciones --------------");
		System.out.println("0.- Puerto de Redireccionamiento Docker");
		System.out.println("1.- Schema Structure");
		System.out.println("2.- Query 01");
		System.out.println("3.- Query 02");
		System.out.println("4.- Query 03");
		System.out.println("5.- Query Get All");
		System.out.println("6.- Exit");
		
		System.out.println("---------------------------------------");
		Scanner in = new Scanner(System.in);
		int num = in.nextInt();
		switch(num){
			case 0:	System.out.println("Indtroduzca el Puerto:");
					portNumber= in.nextInt();
					URL_STRING = "http://localhost:"+portNumber+"/graphiql";
					choose();
					break;
			case 1: try {
						this.sendPost(bq.getSchemaStructure());
					} catch (Exception e) {
						e.printStackTrace();
					}
					choose();
					break;
			case 2:	try {
						this.sendPost(bq.randomQuery01());
					} catch (Exception e) {
						e.printStackTrace();
					}
					choose();
					break;
			case 3: try {
						this.sendPost(bq.randomQuery02());
					} catch (Exception e) {
						e.printStackTrace();
					}
					choose();
					break;
			case 4: try {
						this.sendPost(bq.randomQuery03());
					} catch (Exception e) {
						e.printStackTrace();
					}
					choose();
					break;
			case 5:	try {
						this.sendPost(bq.getAll());
					} catch (Exception e) {
						e.printStackTrace();
					}
					choose();
					break;
			case 6: break;
			default:
					choose();
					break;
		};
		
		
	}

	public static void main(String[] args) throws Exception {
		model_parser p = new model_parser();
		p.init();
		Client client = new Client();
		client.choose();
	}
}
