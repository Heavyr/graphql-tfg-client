let { 
 GraphQLString,
 GraphQLInt, //distances 
 GraphQLObjectType, 
 GraphQLBoolean, 
 GraphQLFloat 
} = require('graphql');

const BusType = new GraphQLObjectType({
	name: 'Malaga', 
	description: 'Tipos de datos -> Autobuses de Malaga', 
	fields: () => ({  
		Bus:{
            name:'nombreLinea',
            type:GraphQLString,
            description:'Letrero del Autobus',
            resolve(obj) {
        		return obj.nombreLinea
            }
    	},
		Parada:{
            name:'nombreParada',
            type:GraphQLString,
            description:'Parada del Autobus',
            resolve(obj) {
        		return obj.nombreParada
            }
    	},
		Longitud:{
            name:'lon',
            type:GraphQLFloat,
            description:'Longitud en coordenadas geográficas',
            resolve(obj) {
        		return obj.lon
            }
    	},
		Direccion:{
            name:'sentido',
            type:GraphQLString,
            description:'Dirección que lleva el Autobus',
            resolve(obj) {
			if (obj.sentido==true)
   				return obj.cabeceraIda
       		else
       			return obj.cabeceraVuelta
            }
    	},
		Latitud:{
            name:'lat',
            type:GraphQLFloat,
            description:'Latitud en coordenadas geográficas',
            resolve(obj) {
        		return obj.lat
            }
    	}
   })
})
module.exports= BusType;