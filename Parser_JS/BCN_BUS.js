let { 
 GraphQLString,
 GraphQLInt, //distances 
 GraphQLObjectType, 
 GraphQLBoolean, 
 GraphQLFloat 
} = require('graphql');

const BusType = new GraphQLObjectType({
	name: 'Barcelona', 
	description: 'Tipos de datos -> Autobuses de Barcelona', 
	fields: () => ({  
		Bus:{
            name:'EQUIPAMENT',
            type:GraphQLString,
            description:'Autobuses que pasan por la parada',
            resolve(obj) {
        		return obj.EQUIPAMENT
            }
    	},
		Parada:{
            name:'NOM_DISTRICTE + NOM_BARRI',
            type:GraphQLString,
            description:'Nombre de distrito',
            resolve(obj) {
        		return obj.NOM_DISTRICTE+"  "+obj.NOM_BARRI
            }
    	},
		Longitud:{
            name:'LONGITUD',
            type:GraphQLFloat,
            description:'Longitud en coordenadas geográficas',
            resolve(obj) {
        		return obj.LONGITUD
            }
    	},
		Latitud:{
            name:'LATITUD',
            type:GraphQLFloat,
            description:'Latitud en coordenadas geográficas',
            resolve(obj) {
        		return obj.LATITUD
            }
    	}
   })
})
module.exports= BusType;