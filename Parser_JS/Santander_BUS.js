let { 
 GraphQLString,
 GraphQLInt, //distances 
 GraphQLObjectType, 
 GraphQLBoolean, 
 GraphQLFloat 
} = require('graphql');

const BusType = new GraphQLObjectType({
	name: 'Santander', 
	description: 'Tipos de datos -> Autobuses de Santander', 
	fields: () => ({  
		Bus:{
        	name:'ayto:numero',
            type:GraphQLString,
            description:'Linea que pasa por la parada',
            resolve(obj) {
              return obj['ayto:numero']
            }
    	},
		Parada:{
        	name:'ayto:parada',
            type:GraphQLString,
            description:'Parada del Autobus',
            resolve(obj) {
              return obj['ayto:parada']
            }
    	},
		Longitud:{
        	name:'wgs84_pos:long',
            type:GraphQLFloat,
            description:'Longitud en coordenadas geográficas',
            resolve(obj) {
              return obj['wgs84_pos:long']
            }
    	},
		Direccion:{
        	name:'ayto:sentido',
            type:GraphQLString,
            description:'Dirección que lleva el Autobus',
            resolve(obj) {
              return obj['ayto:sentido']
            }
    	},
		Latitud:{
        	name:'wgs84_pos:lat',
            type:GraphQLFloat,
            description:'Latitud en coordenadas geográficas',
            resolve(obj) {
              return obj['wgs84_pos:lat']
            }
    	}
   })
})
module.exports= BusType;